export default {
    template:`
    <link rel="stylesheet" href="./app/css/profil.css">
    <h1>Your Profile</h1>
    <p>First name : {{ulogovaniKorisnik.ime}}</p>
    <p>Last name : {{ulogovaniKorisnik.prezime}}</p>
    <p>Username : {{ulogovaniKorisnik.korisnicko_ime}}</p>
`,
    data(){
        return {
            ulogovaniKorisnik: {}
        }
    },
    methods:{
        getUlogovaniKorisnik(){
            axios.get('api/ulogovaniKorisnik').then(response => {
                this.ulogovaniKorisnik = response.data;
                console.log(this.ulogovaniKorisnik);
            })
        }
    },
    created(){
        this.getUlogovaniKorisnik()
    }
}