export default {
    template:`
    <h1>Registration</h1>
<form v-on:submit.prevent="registracija()">
  <div class="mb-3">
    <label class="form-label">Username</label>
    <input type="text" v-model="noviKupac.korisnicko_ime" class="form-control" required>
    <div class="alert alert-danger" role="alert" v-if="neuspjesnaRegistracija" >
      Ovo korisnicko ime je zauzeto!
  </div>
  </div>
  <div class="mb-3">
    <label class="form-label">First name</label>
    <input type="text" v-model="noviKupac.ime" class="form-control" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Last name</label>
    <input type="text" v-model="noviKupac.prezime" class="form-control" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Password</label>
    <input type="password" v-model="noviKupac.lozinka" class="form-control" min=5 required>
  </div>
  <button type="submit" class="btn btn-primary">Register</button>
</form>
`,
    data(){
        return {
            noviKupac: {},
            neuspjesnaRegistracija: false
        }
    },
    methods: {
        registracija: function(){
            axios.post(`/api/registracija`, this.noviKupac).then(response => {
                this.$router.push("/login")
                console.log(this.noviKupac)
            }, _ => {
              this.neuspjesnaRegistracija = true;
            });
        }
    }
}