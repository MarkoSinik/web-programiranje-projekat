import Prodavnica from './components/prodavnica.js'
import RegistracijaForma from './components/registracija.js'
import LoginForma from './components/login.js'
import PrikazFilmova from './components/filmovi.js'
import PregledProfila from './components/pregledProfila.js'
import PromjenaLozinke from './components/promjenaLozinke.js'

axios.interceptors.request.use(config => {
    let token = localStorage.getItem("token");
    Object.assign(config.headers, { "Authorization": `Bearer ${token}` });
    return config;
});




const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: [
        {path: "/registracija", component: RegistracijaForma},
        {path: "/login", component: LoginForma},
        {path: "/filmovi", component: PrikazFilmova},
        {path: "/pregledProfila", component: PregledProfila},
        {path: "/promjenaLozinke", component: PromjenaLozinke}
    ], 
});

const app = Vue.createApp(Prodavnica)
app.use(router)
app.mount("#app")