export default {
    template:`
    
`,
    data(){
        return {
            kupci: []
        }
    },
    methods: {
        refreshKupci(){
            axios.get("/api/kupci").then(response => {
                this.kupci = response.data
            })
        }
    },
    created(){
        this.refreshKupci();
    }
}