export default {
    template:`
<link rel="stylesheet" href="./app/css/filmovi.css">
<h1>MOVIES</h1>
<div class="row">
    <div v-for="f in filmovi" class="column">
            <img v-bind:src="f.slika" height="250">
            <p>{{f.naziv}}</p>
    </div>    
</div>
`,
    data(){
        return {
            filmovi: [],

        }
    },
    methods:{
        refreshFilmovi(){
            axios.get(`api/filmovi`).then(response => {
                this.filmovi = response.data
            })
        }
    },
    created(){
        this.refreshFilmovi();
        
    }
}