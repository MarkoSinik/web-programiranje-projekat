export default {
    template:`
    <form v-on:submit="login()">
  <div class="mb-3">
    <label class="form-label">Username:</label>
    <input type="text" v-model="korisnik.korisnicko_ime" class="form-control" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Password: </label>
    <input type="password" v-model="korisnik.lozinka" class="form-control" required>
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
</form>
<div>
  <p>Don't have an account?<router-link to="/registracija">Register</router-link></p>
</div>
<div class="alert alert-danger" role="alert" v-if="neuspjesanLogin">
  Neispravno korisnicko ime ili lozinka !
</div>
`,
    data(){
        return {
            korisnik: {},
            neuspjesanLogin: false
        }
    },
    methods:{
        login: function(){
            axios.post(`/api/login`, this.korisnik).then((response) => {
              localStorage.setItem("token", response.data)
              console.log(this.korisnik)

              this.$router.push("/filmovi")
            }, _ => {
              this.neuspjesanLogin = true;
              
            })

        }
    }
  
    

}