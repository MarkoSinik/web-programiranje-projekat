export default {
    template:`
    <link rel="stylesheet" href="./app/css/lozinka.css">
    <h1>Change password</h1>
    <form v-on:submit.prevent="promjeniLozinku()">
  <div class="mb-3">
    <label class="form-label">Type in your current password: </label>
    <input type="password"  class="form-control" v-model="staraLozinka" required>
  </div>
  <div class="mb-3">
    <label class="form-label">Type in your new password: </label>
    <input type="password"  class="form-control" v-model="ulogovaniKorisnik.lozinka" required>
  </div>
  <button type="submit" class="btn btn-primary" on:click="promjeniLozinku()">Change password</button>
</form>
    `,
    data(){
        return{
            ulogovaniKorisnik: {},
            staraLozinka: "",
            novaLozinka: ""
        }
    },
    methods: {
        getUlogovaniKorisnik: function(){
            axios.get('api/ulogovaniKorisnik').then(response => {
                this.ulogovaniKorisnik = response.data;
            })
        },
        promjeniLozinku: function(){
            if(this.staraLozinka == this.ulogovaniKorisnik.lozinka){
                axios.put('api/promjenaLozinke', this.ulogovaniKorisnik).then(response => {

                })
            }
        }
    }
}