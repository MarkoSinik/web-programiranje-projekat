export default {
    data(){
        return {
            korisnikUlogovan: false
        }   
    },
    methods: {
        logOut(){
            localStorage.removeItem('token')
            localStorage.removeItem('tipKorisnika')
            this.$router.push('/login')
        },
        provjera(){
            if (localStorage.getItem("token") != null){
                this.korisnikUlogovan = true;
                axios.get('/api/provjera').then(response => {
                    localStorage.setItem("tipKorisnika", response.data)
                  })
            }
        },
       
    },
    created(){
        this.provjera();
    }
}