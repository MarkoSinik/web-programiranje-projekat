from types import MethodDescriptorType
import flask
from flask import Flask
from flask.helpers import send_file
from flask_jwt_extended.utils import get_jwt

from flaskext.mysql import MySQL
import pymysql

from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager

app = Flask(__name__, static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "singidunum"
app.config["MYSQL_DATABASE_PASSWORD"] = "singidunum"
app.config["MYSQL_DATABASE_DB"] = "web_projekat"

mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["JWT_SECRET_KEY"] = "asdasdd dsnsakdn dasd"
jwt = JWTManager(app)


@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route("/api/kupci")
@jwt_required()
def get_all_kupci():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE tip='kupac'")
    kupci = cursor.fetchall()
    return flask.jsonify(kupci)

@app.route("/api/registracija", methods=['POST'])
def registracija():
    noviKupac = dict(flask.request.json)
    noviKupac["tip"] = "kupac"
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * from korisnik WHERE tip='kupac'")
    kupci = cursor.fetchall()
    for k in kupci:
        if k["korisnicko_ime"] == noviKupac["korisnicko_ime"]:
            return "Postoji vec korisnik sa ovim korisnickim imenom !", 409
    cursor.execute("INSERT INTO korisnik(korisnicko_ime, ime, prezime, lozinka, tip) VALUES(%(korisnicko_ime)s, %(ime)s, %(prezime)s, %(lozinka)s, %(tip)s)", noviKupac)
    db.commit()
    return "Uspjesna registracija"


@app.route("/api/login", methods=["POST"])
def login():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * from korisnik WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", flask.request.json)
    korisnik = cursor.fetchone()
    if korisnik is not None:
        acces_token = create_access_token(identity=korisnik["korisnicko_ime"], additional_claims={"roles": korisnik["tip"]})
        
        return flask.jsonify(acces_token), 200

    return "Neispravno korisnicko ime ili lozinka !", 403


@app.route("/api/filmovi", methods=["GET"])
@jwt_required()
def get_all_filmovi():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM film")
    filmovi = cursor.fetchall()
    return flask.jsonify(filmovi)


@app.route("/api/provjera")
@jwt_required()
def provjera_tipa():
    print(get_jwt())
    claims = get_jwt()
    if claims["roles"] == "kupac":
        return "kupac"

    return "admin"
   

@app.route("/api/ulogovaniKorisnik", methods=["GET"])
@jwt_required()
def get_ulogovani_korisnik():
    db = mysql.get_db()
    cursor = db.cursor()
    korisnik = get_jwt()["sub"]
    cursor.execute("SELECT * FROM  korisnik WHERE korisnicko_ime=%s", (korisnik, ))
    korisnik = cursor.fetchone()
    return flask.jsonify(korisnik)

@app.route("/api/promjenaLozinke", methods=["PUT"])
@jwt_required()
def promjena_lozinke():
    ulogovaniKorisnik = dict(flask.request.json)
    db = mysql.get_db()
    cursor = db.cursor()
    korisnicko_ime = get_jwt()["sub"]
    cursor.execute("UPDATE korisnik SET lozinka=%(lozinka)s WHERE korisnicko_ime=%(korisnicko_ime)s", (ulogovaniKorisnik, ))
    korisnik = cursor.fetchone()
    return flask.jsonify(korisnik)

       